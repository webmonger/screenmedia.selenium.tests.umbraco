﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using Screenmedia.Selenium.Tests.Models;

namespace Screenmedia.Selenium.Tests.Tests
{
    public interface IPageTest
    {
        string Name { get; }
        Pen Pen { get; }
        List<PageTestResult> PageErrors { get; set; }
        void PageTest(IWebDriver driver, string url);
        bool CanScreenShot { get; }

        IPageTest GetInstance();
    }
}
