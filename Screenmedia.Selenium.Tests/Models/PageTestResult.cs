﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Screenmedia.Selenium.Tests.Models
{
    public class PageTestResult
    {
        public string Url { get; set; }
        //public Bitmap ScreenCap { get; set; }
        public Rectangle ErrorPosition { get; set; }
        public bool IsError { get; set; }
    }
}
