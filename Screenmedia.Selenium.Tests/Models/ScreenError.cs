﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Screenmedia.Selenium.Tests.Models
{
    class ScreenError
    {
        public Pen Pen { get; set; }
        public Rectangle Rectangle { get; set; }
    }
}
