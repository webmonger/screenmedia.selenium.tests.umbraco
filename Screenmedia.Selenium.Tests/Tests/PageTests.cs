﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Screenmedia.Selenium.Tests.Tests
{
    public class PageTests
    {
        public List<IPageTest> Tests { get; set; }
        public PageTests()
        {
            var type = typeof(IPageTest);
            List<Type> testsTypes = AppDomain.CurrentDomain.GetAssemblies().ToList()
                              .SelectMany(s => s.GetTypes())
                              .Where(p => type.IsAssignableFrom(p) && p.IsClass).ToList();

            Tests = new List<IPageTest>();

            foreach (Type test in testsTypes)
            {
                IPageTest testClass = (IPageTest)Activator.CreateInstance(test);
                Tests.Add(testClass.GetInstance());
            }
        }
    }
}
