﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using Screenmedia.Selenium.Tests.Models;

namespace Screenmedia.Selenium.Tests.Tests
{
    class UserControlErrors : IPageTest
    {
        public bool CanScreenShot
        {
            get { return true; }
        }

        public string Name
        {
            get { return "UserControl errors"; }
        }

        public Pen Pen
        {
            get { return new Pen(Color.Crimson); }
        }

        //public List<IPageTest> PageTests { get; private set; }
        public List<PageTestResult> PageErrors { get; set; }

        private static UserControlErrors _instance;

        public void PageTest(IWebDriver driver, string url)
        {
            try
            {
                By by = By.XPath("//*[contains(.,'usercontrol')]");
                var error = driver.FindElement(by);

                if (error != null)
                {
                    var result = new PageTestResult();
                    result.IsError = true;
                    result.Url = url;

                    IWebElement element = driver.FindElement(by);
                    result.ErrorPosition = new Rectangle(element.Location, element.Size);

                    _instance.PageErrors.Add(result);
                }
            }
            catch{}
        }

        public IPageTest GetInstance()
        {
            _instance = _instance ?? new UserControlErrors();

            if (_instance.PageErrors == null)
                _instance.PageErrors = new List<PageTestResult>();

            return _instance;
        }
    }
}
