﻿using System;
using Umbraco.Core;

namespace Screenmedia.Selenium.Tests
{
    class SiteApplicationBase : UmbracoApplicationBase
    {
        protected override IBootManager GetBootManager()
        {
            return new SiteBootManager(this);
        }

        public void Start(object sender, EventArgs e)
        {
            base.Application_Start(sender, e);
        }
    }
}
