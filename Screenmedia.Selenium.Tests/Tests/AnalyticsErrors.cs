﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using Screenmedia.Selenium.Tests.Models;

namespace Screenmedia.Selenium.Tests.Tests
{
    class AnalyticsErrors : IPageTest
    {
        private const string GoogleAnalyticsCode = "UA-1956184-1";

        public bool CanScreenShot
        {
            get { return false; }
        }

        public string Name
        {
            get { return "Analytics errors " + GoogleAnalyticsCode; }
        }

        public Pen Pen
        {
            get { return new Pen(Color.Blue); }
        }

        //public List<IPageTest> PageTests { get; private set; }
        public List<PageTestResult> PageErrors { get; set; }

        private static AnalyticsErrors _instance;

        public void PageTest(IWebDriver driver, string url)
        {
            try
            {
                By by = By.XPath("//script[contains(.,'" + GoogleAnalyticsCode + "')]");
                var error = driver.FindElement(by);

                if (error != null)
                {
                    var result = new PageTestResult();
                    result.IsError = true;
                    result.Url = url;

                    IWebElement element = driver.FindElement(by);
                    result.ErrorPosition = new Rectangle(element.Location, element.Size);

                    _instance.PageErrors.Add(result);
                }
            }
            catch{}
        }

        public IPageTest GetInstance()
        {
            _instance = _instance ?? new AnalyticsErrors();

            if (_instance.PageErrors == null)
                _instance.PageErrors = new List<PageTestResult>();

            return _instance;
        }
    }
}
