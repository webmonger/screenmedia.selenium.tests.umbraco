﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Screenmedia.Selenium.Tests.Models;

namespace Screenmedia.Selenium.Tests.Tests
{
    class BlogListUITest : IPageTest
    {
        public bool CanScreenShot
        {
            get { return true; }
        }

        public string Name
        {
            get { return "Blog List UI Test"; }
        }

        public Pen Pen
        {
            get { return new Pen(Color.DarkMagenta); }
        }

        //public List<IPageTest> PageTests { get; private set; }
        public List<PageTestResult> PageErrors { get; set; }

        private static BlogListUITest _instance;

        public void PageTest(IWebDriver driver, string url)
        {
            try
            {
                By by = By.XPath("//body[./@class = 'Blog']");
                var error = driver.FindElement(by);

                if (error != null)
                {
                    Console.WriteLine("Is Blog Page");
                    //test for blog list items
                    By byArticles = By.XPath("//div[./@class = 'article']");
                    var articles = driver.FindElements(byArticles);

                    if(articles.Count() == 6)
                    {
                        Console.WriteLine("There are at least 6 Articles");
                        // There should be a get more articles button
                        By getMoreArticles = By.XPath("//button[./@id = 'LoadMoreArticles']");
                        var moreArticlesButton = driver.FindElement(getMoreArticles);

                        if (moreArticlesButton != null)
                        {
                            Console.WriteLine("There are more than 6 Articles");
                            Actions builder = new Actions(driver);
                            Actions action = builder.MoveToElement(driver.FindElement(getMoreArticles));
                            action.Perform(); 
                            Actions action2 = builder.Click(driver.FindElement(getMoreArticles));
                            action2.Perform(); 
                            Thread.Sleep(2000);
                            articles = driver.FindElements(byArticles);
                            Console.WriteLine("There are now " + articles.Count() + " Articles on the page");
                            Thread.Sleep(10000);
                        }
                    }


                    //var result = new PageTestResult();
                    //result.IsError = true;
                    //result.Url = url;

                    //IWebElement element = driver.FindElement(by);
                    //result.ErrorPosition = new Rectangle(element.Location, element.Size);

                    //_instance.PageErrors.Add(result);
                }
            }
            catch { }
        }

        public IPageTest GetInstance()
        {
            _instance = _instance ?? new BlogListUITest();

            if (_instance.PageErrors == null)
                _instance.PageErrors = new List<PageTestResult>();

            return _instance;
        }
    }
}
