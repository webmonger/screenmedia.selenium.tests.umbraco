﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace Screenmedia.Selenium.Tests
{
    [TestFixture]
    public abstract class TestBase
    {
        protected static IWebDriver _driver;
        protected static DateTime _testTime;
        private static List<string> _requestLimiter = new List<string>();
        private static ServiceContext _serviceContext;
        private static IContentService _contentService;
        private static IEnumerable<IContent> _rootContent;
        public string _snapshots;

        [OneTimeSetUp]
        public void TestFixtureSetUp()
        {
            _snapshots = string.Format(@"{0}\{1}\{2}", Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["screenshotDirectory"], ConfigurationManager.AppSettings["url"].Replace("http://", "").Replace("/", ""));
            Directory.CreateDirectory(_snapshots);
            
            string location = string.Format(@"{0}\{1}-{2}", Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["fireFoxProfileRelative"], ConfigurationManager.AppSettings["url"].Replace("http://", "").Replace("/", ""));

            _driver = new FirefoxDriver(new FirefoxProfile(location));
            //_driver = new ChromeDriver(new ChromeDriver(location));

            _testTime = DateTime.Now;
        }


        [OneTimeTearDown]
        public void TestFixtureTearDown()
        {
            try
            {
                _driver.Close();
                //NOTE: This has been added in to prevent the ChromeDriver from crashing
                Thread.Sleep(5000);
                _driver.Quit();
            }
            catch (Exception)
            {
                //ignore incase could not close browser
            }
        }

        [OneTimeSetUp]
        public void TextFixtureSetUp()
        {
            var application = new SiteApplicationBase();
            application.Start(application, new EventArgs());

            var context = ApplicationContext.Current;
            Console.WriteLine("ApplicationContext is available: " + (context != null).ToString());
            //Write status for DatabaseContext
            var databaseContext = context.DatabaseContext;
            Console.WriteLine("DatabaseContext is available: " + (databaseContext != null).ToString());
            //Write status for Database object
            var database = databaseContext.Database;
            Console.WriteLine("Database is available: " + (database != null).ToString());
            Console.WriteLine("--------------------");

            _serviceContext = context.Services;
            _contentService = _serviceContext.ContentService;

            _rootContent = _contentService.GetRootContent();
        }


        protected List<string> GetUrls()
        {
            IList<IContent> nodes = new List<IContent>();
            IList<string> urls = new List<string>();

            GetUmbracoNodes(nodes);
            
            int startLevel = bool.Parse(ConfigurationManager.AppSettings["umbracoHideTopLevelNodeFromPath"]) ? 2 : 1;

            string baseUrl = ConfigurationManager.AppSettings["url"];

            bool limitByDocType = bool.Parse(ConfigurationManager.AppSettings["LimitDocumentTypes"]);

            foreach (IContent node in nodes)
            {
                if (limitByDocType)
                {
                    int allowedOccurances = (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LimitDocumentTypesCount"]))
                                                 ? int.Parse(ConfigurationManager.AppSettings["LimitDocumentTypesCount"])
                                                 : 1;
                    int countOfOcurrances = _requestLimiter.Where(p => p == node.ContentType.Name).Count();
                    if (countOfOcurrances < allowedOccurances)
                    {
                        urls.Add(UmbracoGetNiceUrl(node, startLevel, baseUrl));
                    }
                    _requestLimiter.Add(node.ContentType.Name);
                }
                else
                {
                    urls.Add(UmbracoGetNiceUrl(node, startLevel, baseUrl));
                }
            }

            if(!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LimitUrls"]))
            {
                return urls.Take(int.Parse(ConfigurationManager.AppSettings["LimitUrls"])).ToList();
            }

            Console.WriteLine("Urls: " + urls.Count());

            return urls.ToList();
        }

        private void GetUmbracoNodes(IList<IContent> nodes)
        {
            foreach (var content in _rootContent)
            {
                nodes.Add(content);
                AddChildNodes(content, nodes);
            }
        }

        private static void AddChildNodes(IContent parent, IList<IContent> nodes)
        {
            var descendants = _contentService.GetDescendants(parent).Where(x=>x.Published).ToList();
            if (descendants.Any())
            {
                foreach (IContent node in descendants)
                {
                    nodes.Add(node);
                    AddChildNodes(node, nodes);
                }
            }
        }

        private static string UmbracoGetNiceUrl(IContent node, int startNodeDepth, string currentDomain)
        {
            string url;

            if (node.Level < startNodeDepth)
            {
                url = "/";
            }
            else
            {
                url = node.Name.FormatUrl().ToLower() + "/";
            }

            if(node.ParentId > 0)
            {
                url = "/" + AddContentToUrl(_contentService.GetById(node.ParentId), url);
            }
            
            return url;
        }

        private static string AddContentToUrl(IContent node, string url)
        {
            if(node.ParentId > 0)
            {
                url = node.Name.FormatUrl().ToLower() + "/" + url;
                url = AddContentToUrl(_contentService.GetById(node.ParentId), url);
            }
            return url;
        }

        public Bitmap TakeScreenshot()
        {
            // 1. Make screenshot of all screen
            Screenshot screenshot = ((ITakesScreenshot)_driver).GetScreenshot();
            var bmpScreen = new Bitmap(new MemoryStream(screenshot.AsByteArray));

            // 2. Get screenshot of specific element
            //IWebElement element = _driver.FindElement(by);
            //var cropArea = new Rectangle(element.Location, element.Size);

            return bmpScreen;//.Clone(cropArea, bmpScreen.PixelFormat);
        }
    }
}