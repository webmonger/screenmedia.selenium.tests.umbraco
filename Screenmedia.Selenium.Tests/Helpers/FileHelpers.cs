﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace Screenmedia.Selenium.Tests.Helpers
{
    public static class FileHelpers
    {
        public static void OutputUrlList(DateTime testTime, string name, IEnumerable<string> urls)
        {
            string dateTime = testTime.ToString("yyyyMMdd-HHmmss");
            string baseDirectory = Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory) + "..\\..\\";
            string directory = string.Format("{0}\\{1}", baseDirectory + OutputDirectory.TrimEnd(new[] { '\\' }), dateTime);

            CreateDirectory(directory);

            string path = string.Format(@"{0}\{1}.csv", directory, name);

            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.AutoFlush = true;

                foreach (string url in urls)
                {
                    sw.WriteLine(url);
                }

                sw.WriteLine();
                sw.Close();
            }
        }
        
        public static void OutputUrlList(DateTime testTime, string name, string output)
        {
            string dateTime = testTime.ToString("yyyyMMdd-HHmmss");
            string baseDirectory = Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory);
            string directory = string.Format("{0}\\{1}", baseDirectory, OutputDirectory.TrimEnd(new[] { '\\' }));

            CreateDirectory(directory);

            string path = string.Format(@"{0}\{2}-{1}.csv", directory,
                                        ConfigurationManager.AppSettings["url"].Replace("http://", "").Replace("/", ""),
                                        dateTime);

            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.AutoFlush = true;

                sw.Write(output);

                sw.WriteLine();
                sw.Close();
            }
        }

        public static void CreateDirectory(string directory)
        {
            if (Directory.Exists(directory))
                return;

            Directory.CreateDirectory(directory);
        }

        public static string OutputDirectory
        {
            get
            {
                return ConfigurationManager.AppSettings["OutputDirectory"];
            }
        }
    }
}