﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Screenmedia.Selenium.Tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using Screenmedia.Selenium.Tests.Models;
using Screenmedia.Selenium.Tests.Tests;

namespace Screenmedia.Selenium.Tests
{
    [TestFixture]
    public class PageTest : TestBase
    {
        private IList<string> _loadTime = new List<string>();

        [TearDown]
        public void TearDown()
        {
            Thread.Sleep(100);
        }

        [Test]
        public void Check_For_Errors()
        {
            var testUrls = new TestUrls();
            testUrls.Urls = GetUrls();

            Assert.IsNotEmpty(testUrls.Urls);

            var pageTests = new PageTests();

            List<IPageTest> myPageTests = pageTests.Tests;

            foreach (string url in testUrls.Urls)
            {
                Console.WriteLine("Url: " + url);
                DateTime startTime = DateTime.UtcNow;
                _driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["url"] + url);
                DateTime endTime = DateTime.UtcNow;

                TimeSpan span = endTime - startTime;
                _loadTime.Add(span.TotalSeconds.ToString());

                testUrls.ScreenCaptures.Add(TakeScreenshot());

                foreach (var myPageTest in myPageTests)
                {
                    try
                    {
                        myPageTest.PageTest(_driver, url);
                    }catch{}
                }
            }

            OuputErrors(myPageTests, testUrls);
            CheckAsserts(myPageTests);
        }

        //private void Check_For_Other_Errors(string url)
        //{
        //    try
        //    {
        //        var error = _driver.FindElement(By.XPath("//*[contains(.,'error')]"));

        //        if (error != null)//  && !_otherErrors.Contains(url) && !_userControlErrors.Contains(url) && !_xsltErrors.Contains(url))
        //            _otherErrors.Add(url);
        //    }
        //    catch (Exception)
        //    {
        //        //if (!_razorErrors.Contains(url) && !_userControlErrors.Contains(url) && !_xsltErrors.Contains(url))
        //        //    _otherErrors.Add(url);
        //    }
        //}

        //private void Check_For_Test_Content(string url)
        //{
        //    try
        //    {
        //        By by = By.XPath("//p[contains(.,'Lorem')]");
        //        var error = _driver.FindElement(by);

        //        if (error != null)//&& !_razorErrors.Contains(url) && !_userControlErrors.Contains(url) && !_xsltErrors.Contains(url))
        //        {
        //            //Bitmap shot = TakeScreenshot(by);
        //            //shot.SaveJPG100(_snapshots + "\\" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + "-" + url.Replace("/", "-") + ".jpg");

        //            _testContent.Add(url);
        //        }

        //        if(!_testContent.Contains(url))
        //        {
        //            var lipsumError = _driver.FindElement(By.XPath("//*[contains(.,'darren')]"));

        //            if (lipsumError != null)
        //            {
        //                _testContent.Add(url);
        //            }
        //        }

        //    }
        //    catch (Exception)
        //    {
        //        //if (!_razorErrors.Contains(url) && !_userControlErrors.Contains(url) && !_xsltErrors.Contains(url))
        //        //    _otherErrors.Add(url);
        //    }
        //}

        private void CheckAsserts(List<IPageTest> myPageTests)
        {
            foreach (var myPageTest in myPageTests)
            {
                Assert.IsEmpty(myPageTest.PageErrors);
            }
        }

        private void OuputErrors(List<IPageTest> tests, TestUrls testUrls)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("url,loadtime");
            foreach (var pageTest in tests)
            {
                stringBuilder.Append("," + pageTest.Name);
            }

            stringBuilder.Append("\n");

            var errors = new List<ScreenError>();

            //List<List<PageTestResult>> results = new List<List<PageTestResult>>();
            for (int i = 0; i < testUrls.Urls.Count(); i++)
            {
                string url = testUrls.Urls.Skip(i).First();
                stringBuilder.AppendFormat("{0},{1}", url, _loadTime.Skip(i).First());

                foreach (var pageTest in tests)
                {
                    if (pageTest.PageErrors != null)
                    {
                        bool isError = IsInArray(url, pageTest.PageErrors);
                        stringBuilder.Append("," + ((isError) ? "x" : string.Empty));
                        if (pageTest.PageErrors.Count() > 0)
                        {
                            IEnumerable<PageTestResult> results = pageTest.PageErrors.Where(e => e.Url == url);
                            if (results.Count() > 0)
                            {
                                PageTestResult result = results.First();
                                errors.Add(new ScreenError() {Rectangle = result.ErrorPosition, Pen = pageTest.Pen});
                            }
                        }
                    }
                }
                stringBuilder.Append("\n");

                WriteScreenshot(testUrls.ScreenCaptures.Skip(i).First(), errors, url);
            }

            FileHelpers.OutputUrlList(_testTime, "Exceptions", stringBuilder.ToString());
        }

        private void WriteScreenshot(Bitmap screenShot, List<ScreenError> errors, string url)
        {
            Graphics g1 = Graphics.FromImage(screenShot);
            //errors.
            foreach (var error in errors)
            {
                g1.DrawRectangle(error.Pen, error.Rectangle); //draw a rectangle to the bitmap
            }
            string file = _snapshots + "\\" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + "-" + url.Replace("/", "-") +
                          ".jpg";
            screenShot.SaveJPG100(file);
        }

        private bool IsInArray(string url, IEnumerable<PageTestResult> testResults)
        {
            if(testResults.Any(r => r.Url == url))
            {
                return true;
            }
            return false;
        }
    }
}

