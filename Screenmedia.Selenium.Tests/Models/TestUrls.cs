﻿using System.Collections.Generic;
using System.Drawing;

namespace Screenmedia.Selenium.Tests.Models
{
    public class TestUrls
    {
        public TestUrls()
        {
            ScreenCaptures = new List<Bitmap>();
        }

        public List<string> Urls { get; set; }
        public List<Bitmap> ScreenCaptures { get; set; }
    }
}
