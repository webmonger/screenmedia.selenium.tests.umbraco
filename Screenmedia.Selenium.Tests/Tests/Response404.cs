﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using Screenmedia.Selenium.Tests.Models;

namespace Screenmedia.Selenium.Tests.Tests
{
    class Response404 : IPageTest
    {
        public bool CanScreenShot
        {
            get { return true; }
        }

        public string Name
        {
            get { return "404 errors"; }
        }

        public Pen Pen
        {
            get { return new Pen(Color.CadetBlue); }
        }

        //public List<IPageTest> PageTests { get; private set; }
        public List<PageTestResult> PageErrors { get; set; }

        private static Response404 _instance;

        public void PageTest(IWebDriver driver, string url)
        {
            try
            {
                By by = By.XPath("//body[contains(.,'404')]");
                var error = driver.FindElement(by);

                if (error != null)
                {
                    var result = new PageTestResult();
                    result.IsError = true;
                    result.Url = url;

                    IWebElement element = driver.FindElement(by);
                    result.ErrorPosition = new Rectangle(element.Location, element.Size);

                    _instance.PageErrors.Add(result);
                }
            }
            catch (Exception)
            {
                //_userControlErrors.Add(url);
            }
        }

        public IPageTest GetInstance()
        {
            _instance = _instance ?? new Response404();

            if (_instance.PageErrors == null)
                _instance.PageErrors = new List<PageTestResult>();

            return _instance;
        }
    }
}
