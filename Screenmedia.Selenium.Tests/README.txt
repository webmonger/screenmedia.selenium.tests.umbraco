Quick Intro to Umbraco site tester

Extract Zip file

Open sln in Visual Studio

Configure App.config
- Change connectionstrings to working Umbraco 6 database
- Setup Limits (If you wish!)
- Set the correct URL of the site in the web.config

Open PageTest.cs
- Run the Check_for_errors Unit test

Start the test runner

Yay we have errors :)

Open the test output file 

Show me what errors happened on the site
Url
How long did the page take to load
Razor errors
UserControl errors
XSLT errors
Other, Exceptions, GA code missing
Test Content contains('lorem')








